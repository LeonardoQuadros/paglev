# -*- encoding : utf-8 -*-


if Setting.table_exist?

  Setting.defaults['CONFIGURACOES.site']              			    = ""
  Setting.defaults['CONFIGURACOES.title']              			    = "Nabrasa Rotisserie"
  Setting.defaults['CONFIGURACOES.descricao']              			= "Nabrasa Rotisserie"
  Setting.defaults['CONFIGURACOES.keys']  						          = "Nabrasa Rotisserie"
  	#Informações
    Setting.defaults['CONFIGURACOES.email']    						      = ""
    Setting.defaults['CONFIGURACOES.telefone']    						  = ""
    Setting.defaults['CONFIGURACOES.site_link']    						  = ""
    Setting.defaults['CONFIGURACOES.facebook']    						  = ""

  Setting.defaults['CONFIGURACOES.sobre']                       = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
  Setting.defaults['CONFIGURACOES.atributos']                       = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

  Setting.defaults['CONFIGURACOES.informacoes_importantes']              		  		= "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
  Setting.defaults['CONFIGURACOES.additional_informations']                       = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
end