require_relative 'boot'

require 'rails/all'
require 'net/http'
require 'uri'
require "yaml"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Intranet
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.time_zone = 'Brasilia'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = :'pt-BR'

    # Configure the default encoding used in templates
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    config.active_record.default_timezone = :local

    # Autoload ckeditor models folder
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)

    # Loadin your lib files
    config.autoload_paths += %W(#{config.root}/lib)

    # Loadin fonts
    config.assets.paths += %W(#{config.root}/app/assets/fonts)

    options = {
      :enabled => true,
      :remove_multi_spaces => true,
      :remove_comments => true,
      :remove_intertag_spaces => false,
      :remove_quotes => false,
      :compress_css => false,
      :compress_javascript => false,
      :simple_doctype => false,
      :remove_script_attributes => false,
      :remove_style_attributes => false,
      :remove_link_attributes => false,
      :remove_form_attributes => false,
      :remove_input_attributes => false,
      :remove_javascript_protocol => false,
      :remove_http_protocol => false,
      :remove_https_protocol => false,
      :preserve_line_breaks => false,
      :simple_boolean_attributes => false,
      :compress_js_templates => false
    }

    config.i18n.default_locale = :'pt-BR'
	console do
	  ActiveRecord::Base.connection
	end


  end
end


