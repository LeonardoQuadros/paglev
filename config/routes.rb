Rails.application.routes.draw do

#=============================================================================
# ================================ USER ======================================
#=============================================================================

#=============================================================================
# ================================ PÚBLICO ===================================
#=============================================================================

  devise_for :users, :controllers => { registrations: 'registrations',
                                        :omniauth_callbacks => "callbacks" }


  authenticated :user do
    root 'user/pages#home', as: :authenticated_root
  end

  controller :users do
  end
  resources :users

  controller :pages do
    get '/home' => :home

  end 

  root 'pages#home'


get '*path' => redirect('/')
end