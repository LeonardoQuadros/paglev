class PagesController < PublicController

  def home
  	unless session[:opcao].blank?
  		@pratos = Prato.where(categoria_id: session[:opcao].to_i)
  	end
  end



  def menu_opcao
  	@opcao = params[:opcao].to_i
  	session[:opcao] = params[:opcao].to_i

  	@pratos = Prato.where(categoria_id: @opcao)

    respond_to do |format|
      format.js
    end
  end

end
