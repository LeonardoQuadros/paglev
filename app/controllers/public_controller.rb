class PublicController < ApplicationController
  layout 'publico'

  before_filter :seo



  private

    # Cria as meta tags.
    def seo
      set_meta_tags description:  "#{Setting['CONFIGURACOES.descricao'].blank? ? 'Duo de gastronomia e bem-estar, a Nabrasa Rotisserie visa atender a quem segue uma dieta saudável e busca por refeições saborosas.' : Setting['CONFIGURACOES.descricao']}",
                    title:        "#{Setting['CONFIGURACOES.title'].blank? ? 'Nabrasa Rotisserie' : Setting['CONFIGURACOES.title']}",
                    keywords:     "#{Setting['CONFIGURACOES.keys'].blank? ? 'Nabrasa Rotisserie,rotisserie, nabrasa, restaurante são carlos, restaurante sao carlos, restaurante nabrasa, restaurante rotisserie, dieta saudavel, melhor restaurante sao carlos' : Setting['CONFIGURACOES.keys']}",
                    author:       'Nabrasa Rotisserie',
                    generator:    'Nabrasa Rotisserie',
                    charset:      "utf-8",
                    og: {
                      site_name:    "#{Setting['CONFIGURACOES.site'].blank? ? 'Nabrasa Rotisserie' : Setting['CONFIGURACOES.site']}",
                      title:        "#{Setting['CONFIGURACOES.title'].blank? ? 'Nabrasa Rotisserie' : Setting['CONFIGURACOES.title']}",
                      type:         'article',
                      description:  "#{Setting['CONFIGURACOES.descricao'].blank? ? 'Duo de gastronomia e bem-estar, a Nabrasa Rotisserie visa atender a quem segue uma dieta saudável e busca por refeições saborosas.' : Setting['CONFIGURACOES.descricao']}",
                      url:          root_url[0..-2],
                      image:        root_url[0..-2] + ActionController::Base.helpers.image_url('logo.png')
                    }
    end
end
